# yyyy-mm-dd: subject

## Meeting Information
|                 |                         |
|-----------------|-------------------------|
| **Subject:**    | subject |
| **Date/Time:**  | dd.mm.yyyy, HH:MM-HH:MM |
| **Location:**   | MS Teams |
| **Note Taker:** | Stefan Lüthi |

## Participants

 - [ ] Stefan Lüthi (maintainer)
 - [ ] Roger Weber (advisor)

## Agenda

1. xy


## Meeting
---
1. **xy**

## Next Meeting
according to sprint cycle