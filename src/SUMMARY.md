# Summary

[Meetings](meetings.md)
- [2022-06-13: Espresso Machine Demo and Bern Kernel](meetings/2022-06-13.md)
- [2022-04-05: Espresso Machine Demo MVP](meetings/2022-04-05.md)
- [2022-03-07: Espresso Machine Modifications done](meetings/2022-03-07.md)
- [2022-02-25: Bern Kernel v0.3.0](meetings/2022-02-25.md)
- [2022-01-14: Hardware order](meetings/2022-01-14.md)
- [2021-06-04: Bern Kernel v0.2.0](meetings/2021-06-04.md)
- [2021-05-07: Bern Kernel v0.1.0](meetings/2021-05-07.md)
