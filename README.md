# Meetings

The meeting notes are available as a web-book at [meetings.bern-rtos.org](https://meetings.bern-rtos.org).

## Prerequisites

- `mdbook`

## Build

```bash
mdbook build
```

The HTML files can be found in `./mdbook/book`.
